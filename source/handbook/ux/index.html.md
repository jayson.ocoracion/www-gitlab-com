---
layout: markdown_page
title: "UX Team"
---

## UX Guide

The [UX Guide](https://docs.gitlab.com/ce/development/ux_guide/) documents our principles, approach, and perspective to the experience of GitLab. Help keep this document up to date and correct by making [merge requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/).

## UX Strategy

The vision of GitLab is far from finished and there are a lot of emergent properties that will allow us to simplify. We believe in iterative improvements and value aggressive change. We will get some things wrong and quickly strive to make them right. We have a long way to go, that is why we are taking big steps.

Please see the [2017 UX Strategy](/handbook/ux/strategy) to view the evolving UX vision for GitLab.

## UX Workflow 

### Designer

Issues should be tagged with 'UX' if UX work is required.

1. Work on issues tagged with 'UX', in the following order:
    1. **Scheduled for current release**
    2. **Scheduled for next release** 
    3. Tagged with **Coming soon**, **To schedule**, or **Shortlist**
    4. Issues with high **community involvement** (number of comments or thumbsup)
    5. Everything else
1. UX issues have a tendency to expand in scope. Aggressively split off new issues, ideas, and concepts into their own issues. Large issue become really challenging to drive decisions in and make progress on. If you are ever unsure how to split apart large issues, work with the UX Lead.
	* Developers should be able to ship a product within one life cycle. If a feature is too large to ship within one release, work together to determine the best way of splitting the feature into smaller segments.
	* Bring developers into the conversation early. Ask for feedback on how to split up features while still maintaining the integrity of the UX.
	* When breaking up features into smaller parts, make sure that the end design goal is known. Giving the team the full picture will help developers write code aimed at achieving that goal in the future.
1. Before you hand off the work, make sure to update the issue description with the single source of truth. Update the issue description if the single source of truth changes while the work is going on. If the developer implementing the issue ever has any questions on what they should implement, they can ask the designer to update the issue description with the design.
1. Once UX work is completed and feedback addressed, unassign yourself and remove the UX label. 
   * If the issue is scheduled for a milestone, add the next [workflow label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#workflow-labels) needed to progress the issue. Typically, this is the Frontend label.
   * If the issue is not scheduled, and is a bug or a minor, non-direction feature proposal, mention the responsible team lead to schedule it.
   * If the issue is not scheduled, and is a major, direction feature proposal, mention a product manager to [prioritize and schedule it](https://about.gitlab.com/handbook/engineering/workflow/#scheduling-issues)
1. Continue to follow the issue, addressing any additional UX issues that come up.

### Researcher

**How do I raise a research request for the UX researcher?**

1. Within the [UX Research project](https://gitlab.com/gitlab-org/ux-research), raise a new issue using the research template. 
1. If you want to discuss any aspect of the research, you should add comments to the issue you created in step 1.
1. Add the label `backlog` to the issue. 
1. If there are any related issues in the GitLab CE or EE project, ensure they have the label of `UX Research`.

**What is the UX researcher workflow?**

1. Work with the UX team to determine what should be researched.
1. Create a meta issue within the UX research project. 
1. Label the meta issue with the area of GitLab you are testing (for example, `navigation`) and the status of the issue (`in progress`).
1. Mark the meta issue as `confidential` until the research is completed so it doesn’t influence user behaviour. If there are any associated issues in the UX research project, you will also have to mark these as `confidential` too.
1. Conduct the research. 
1. Document the findings and recommendations within the meta issue. 
1. Unmark the meta issue and any associated issues within the UX research project as `confidential`. 
1. Update the status of the meta issue and any associated issues within the UX research project to `done`.
1. Within the meta issue, encourage discussion between yourself, UX designers and product owners about which findings should be turned into issues within the GitLab CE or EE project.
1. Create the agreed, new issues within the GitLab CE or EE project. Link the issues back to the meta issue within the UX Research project. Label the new issues as [appropriate](https://about.gitlab.com/handbook/engineering/workflow/#labelling-issues).
1. Close the meta issue and associated issues within the UX Research project.


## UX on Social Media

It is encouraged to share UX designs and insight on social media platforms such as Twitter and Dribbble.

### Twitter

You can contribute design-related posts to our `@gitlab` Twitter handle by adding your tweet to our [UX Design Twitter spreadsheet](https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit).

1. Add a new row with your tweet message, a relevant link, and an optional photo.
1. Ensure that your tweet is no more than 140 characters. If you are including a link, ensure you have enough characters and consider using a [link shortening service](https://bitly.com/).
1. The UX Lead will check the spreadsheet at the beginning of each week and schedule any tweets on Tweetdeck.
1. Once a tweet is scheduled, the tweet will be moved to the "Scheduled" tab of the spreadsheet.

### Dribbble

GitLab has a [team dribbble account](https://dribbble.com/GitLab) where you can add work in progress, coming soon, and recently released works.

* If a dribbble post has a corresponding open issue, link to the issue so designers can contribute on GitLab.
* Share the dribbble post on twitter, along with a link to the corresponding open issue if applicable.
* If you are not a member of the GitLab dribbble team and would like to be, contact the UX Lead to grant you membership.

*Also see the [basics of GitLab development in the developer onboarding](https://about.gitlab.com/handbook/developer-onboarding/#basics-of-gitlab-development).*
